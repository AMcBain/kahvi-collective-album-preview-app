"use strict";

if (!window.Audio)
{
    $(function ()
    {
        document.body.className = "ahoooga";
        document.body.innerHTML = "<h1>Bad luck!</h1><p>No <a href='https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio'>" +
                "HTML5 audio</a> support found. Please upgrade your browser to a <a href='http://caniuse.com/#feat=audio'>version " + 
                "which does</a> or install a different browser with support for the audio tag.</p>";
    });
    throw new Error("No <audio> support found.");
}

// UniversalConstants (at least in this universe)
var UC = {
    plural : function (value, unit, singular)
    {
        if (value === 1)
        {
            return value + " " + unit + (singular || "");
        }
        return value + " " + unit + "s";
    }
};

$(function ()
{
    var wait = $("#search em"), error = $("#search strong"), search = $("#search"), albums = $("#search ul"), lastTimeout,
            lastWaitTimeout, lastAjax;

    checkWaitStyles(wait);
    wait.hide();

    // Stupid way to wait for fonts to load cross-browser. Too lazy to do it the right way.
    setTimeout(function ()
    {
        var input, test, height, size, left, support, about;

        input = $("#search input");
        test = $("#test");

        UC.fontSize = parseInt(test.css("fontSize"));
        UC.lineHeight = test.height();
        UC.inputHeight = input[0].offsetHeight;

        height = UC.inputHeight - 2;
        $("progress,meter").css("height", UC.inputHeight + "px");
        $("#album").attr("data-height", 2 * UC.fontSize + height + 2 + "px");

        support = $("meter");
        if (!("value" in support[0]))
        {
            support.css("display", "none");
        }
        support = !support[0].style.display;

        $("#album > span:first-child").css("fontSize", 1.1118 * height - 1.6968 + "px")
            .css("top", UC.fontSize + "px").css("left", UC.fontSize + "px");

        size = 1.1854 * height - 0.4416;
        $("#album > progress + span").css("fontSize", size)
            .css("top", UC.fontSize + "px").css("right", 1 * UC.fontSize + .5 * UC.fontSize * support + "px")
            .css("marginRight", $("meter").outerWidth() * support + "px").css("width", size + "px");

        left = 1.5 * UC.fontSize + 1.1349 * height - 2.8562;
        $("progress").css("left", left + "px")
            .css("width", search.width() - left - $("meter").width() * support - 4 - size - .5 * UC.fontSize * support + "px");

        support = $("progress");
        if (!("value" in support[0]))
        {
            support.css("display", "none");
        }

        $("#album div:first-of-type").css("top", UC.inputHeight + "px");

        search.css("height", UC.inputHeight + "px");
        search.data("height", search.height());

        // Change default top to one more appropriate so it doesn't overlap the input.
        $("#search div").css("top", UC.fontSize * 2 + UC.inputHeight + "px");

        about = $("#about");
        test.css("width", search.width());
        test.css("padding", "1em");
        test.html(about.html());
        about.data("height", test.height() + "px");

        // See the input event handler below, which rejects events when the line-height isn't set. So if
        // this is reached and there's a value, we should fire an event to let it know to go now.
        if (input.val())
        {
            input.triggerHandler("input");
        }
    }, 1000);

    // Off by default.
    $("#album,#about").css("display", "none");
    $("#about").find("noscript").remove();

    // Test formats available from Kahvi (get from server instead?)
    UC.formats = {};
    $.each([ "ogg", "mpeg" ], $.proxy(function (_, value)
    {
        UC.formats[value] = this.canPlayType && this.canPlayType("audio/" + value);
    }, document.createElement("audio")));

    $("nav li[data-id]").click(function ()
    {
        var active, height;

        if (!this.className)
        {
            height = $("body > div.active").height();

            active = $("nav li.active");
            active.removeClass("active");
            $("#" + active.data("id")).removeClass("active").css("display", "none");

            $(this).addClass("active");
            active = $("#" + this.getAttribute("data-id"));
            active.addClass("active");

            active.data(height);
            active.css("height", height + "px");
            active.css("display", "");

            // If you do it in the same context, the browser won't transition the display and it will
            // jump instead of smoothly transitioning larger or smaller.
            setTimeout(function ()
            {
                active.css("height", active.data("height"));
            }, 100);
        }
    });

    $("#search input").on("input", function ()
    {
        clearTimeout(lastTimeout);
        error.css("display", "");

        // The albums list won't get the right size if we don't have the line-height yet. This can be
        // encountered by clicking the input field really fast after the page loads and typing a letter.
        if (!UC.lineHeight)
        {
            return;
        }

        var value = this.value;
        lastTimeout = setTimeout(function ()
        {
            if (lastAjax)
            {
                clearTimeout(lastWaitTimeout);
                lastAjax.abort();
            }

            // If after 1s were still waiting, show the throbber. Prevents a scrollbar from showing up
            // due to the throbber itself while the parent is still expanding.
            lastWaitTimeout = setTimeout(function ()
            {
                if (albums.height() < 85)
                {
                    search.css("height", UC.fontSize * 2 + UC.inputHeight + 85 + "px");
                    search.data("height", search.height() + "px");
                }
                wait.show();
                wait.css("animationPlayState", "running");
            }, 1000);

            lastAjax = $.ajax("/search/?value=" + encodeURIComponent(value), {
                success: function (data)
                {
                    clearTimeout(lastWaitTimeout);

                    albums.empty();
                    albums.append($.map(data, function (value)
                    {
                        var li = [ "<li data-id='", value.id, "'" ];

                        li.push(UC.formats[value.format] ? "" : " class='disabled'");
                        li.push(UC.formats[value.format] ? "" : " title='This album is not available in a format this browser supports'");
                        li.push(">");

                        li.push("<span>", value.artist, " | ");
                        li.push(value.tracks < 10 ? "\u2007" : "");
                        li.push(UC.plural(value.tracks, "track", "\u2004\u2006"));
                        li.push("</span>");

                        li.push("<span>", value.title, "</span></li>");
                        return li.join("");
                    }).join(""));
                    albums.parent().scrollTop(0);

                    var height = Math.min(17, data.length) * UC.lineHeight + UC.inputHeight + !!data.length * UC.fontSize + "px";
                    search.data("height", height);
                    search.css("height", height);

                    wait.css("animationPlayState", "");
                    wait.hide();
                },
                error: function (xhr, status)
                {
                    var height;
                    clearTimeout(lastWaitTimeout);

                    if (status !== "abort")
                    {
                        height = UC.fontSize * 2 + UC.inputHeight + 85 + "px";
                        search.css("height", height);
                        search.data("height", height);

                        wait.css("animationPlayState", "");
                        wait.hide();

                        albums.empty();
                        error.css("display", "table");
                    }
                }
            }); // ajax
        }, 200);
    }).val("");
});

$(function ()
{
    var wait, error, album, tracks, button, progress, meter, mute, pox, mox, info, title, artist, link, icons, setPlayButtonState,
            setSliderValue, release, releaseName, player, lastTimeout, lastWaitTimeout, lastAjax, seek, mouseDown;

    wait = $("#album em");
    checkWaitStyles(wait);
    wait.hide();

    error = $("#album strong");
    album = $("#album");
    tracks = $("#album ol")

    info = $("#album div:last-child");
    title = info.find("span:first-child");
    artist = title.next();
    link = info.find("a");

    icons = {
          mute: "\uf021",
        volume: "\uf022",
          play: "\uf023",
         pause: "\uf024"
    };

    // Play/pause
    setPlayButtonState = function (button, state)
    {
        if (error.is(":hidden"))
        {
            if (state === icons.play)
            {
                button.title = "pause";
                button.innerHTML = icons.pause;

                if (player)
                {
                    player.play();
                }
                else
                {
                    createAudio(tracks.children()[0]);
                }
            }
            else
            {
                button.title = "play";
                button.innerHTML = icons.play;

                if (player)
                {
                    player.pause();
                }
            }
        }
    };

    button = $("#album > span:first-child").click(function ()
    {
        setPlayButtonState(this, this.innerHTML);
    }).text(icons.play)[0];

    // Mute
    mute = $("#album > progress + span").click(function ()
    {
        if (error.is(":hidden"))
        {
            if (this.innerHTML === icons.mute)
            {
                this.className = "";
                this.title = "mute volume";
                this.innerHTML = icons.volume;
                player.muted = false;
            }
            else
            {
                this.className = "none";
                this.title = "unmute volume";
                this.innerHTML = icons.mute;
                player.muted = true;
            }
        }
    }).text(icons.volume)[0];

    setSliderValue = function (event, element, box)
    {
        // Browsers don't provide a reliable set of properties to get the location of the mouse
        // relative to the element clicked, so unfortunately we're left subtracting the x coordinate
        // part of the pair relative to the page (not viewport, we want to account for scrolling) from
        // the element's bounding box.
        var x = Math.min(event.pageX - Math.floor(box.left));

        if (x <= element.clientWidth)
        {
            element.value = x / element.clientWidth * element.max;
        }
    };

    // Progress (seeking)
    progress = $("#album progress").on({
        mousedown: function (event)
        {
            if (player && player.readyState !== player.HAVE_NOTHING)
            {
                mouseDown = this;
                setSliderValue(event, this, pox);
            }
        },
        mousemove: function (event)
        {
            if (mouseDown === this && player)
            {
                setSliderValue(event, this, pox);
                this.innerHTML = formatLongDuration(progress.value);
            }
        }
    })[0];

    // Meter (volume)
    meter = $("#album meter").on({
        mousedown: function (event)
        {
            if (error.is(":hidden"))
            {
                mouseDown = this;
                setSliderValue(event, this, mox);
            }
        },
        mousemove: function (event)
        {
            if (mouseDown === this)
            {
                setSliderValue(event, this, mox);
                this.innerHTML = Math.floor(this.value * 100) + "% volume";

            }
        }
    })[0];

    // Mouse up or click won't fire on the element itself if the mouse is outside it. This traps those events.
    $(document.body.parentNode).mouseup(function (event)
    {
        if (mouseDown === progress && player)
        {
            player.currentTime = progress.value;
            mouseDown = false;
        }
        else if (mouseDown === meter)
        {
            // Allow setting volume without having anything currently playing (or paused).
            if (player)
            {
                player.volume = meter.value;
            }
            meter.innerHTML = Math.floor(meter.value * 100) + "% volume";

            mouseDown = false;
        }
    });

    function nixPlayer ()
    {
        if (player)
        {
            player.pause();
            player = null;

            progress.value = 0;
            progress.innerHTML = "no progress";

            setPlayButtonState(button, icons.pause);
        }
    }

    function formatLongDuration (time)
    {
        var minutes, seconds;

        minutes = Math.floor(time / 60);
        seconds = Math.floor(time - minutes * 60);

        if (!minutes)
        {
            return UC.plural(seconds, "second");
        }
        return UC.plural(minutes, "minute") + " and " + UC.plural(seconds, "second");
    }

    function formatDuration (time)
    {
        var minutes, seconds;

        minutes = Math.floor(time / 60);
        seconds = Math.floor(time - minutes * 60);

        return (minutes === 0 && "0") + minutes + ":" + ((seconds < 10 && "0") + seconds);
    }

    // Something about Android and not supporting setting the src property more than once with success.
    // I couldn't exactly verify that, as areweplayingyet.org's data tables seem to have been nuked.
    function createAudio (element, preload)
    {
        var volume = 1;

        if (player)
        {
            volume = player.volume;
            player.element.className = "";
            player.pause();
        }
        else if ("value" in meter)
        {
            volume = meter.value;
        }

        player = new Audio();
        player.volume = volume;
        player.element = element;

        player.addEventListener("durationchange", function ()
        {
            progress.max = this.duration;

            if (this.duration !== Infinity && !element.firstChild.innerHTML)
            {
                element.firstChild.innerHTML = formatDuration(this.duration);

                // Cache on server.
                $.ajax("/release/" + release + "/track/" + element.getAttribute("data-id") + "/length/" + this.duration);
            }
        });

        player.addEventListener("timeupdate", function ()
        {
            if (!mouseDown && player)
            {
                progress.value = this.currentTime;
                progress.innerHTML = formatLongDuration(this.currentTime);
            }
        });

        player.addEventListener("ended", function ()
        {
            element.className = "";

            do
            {
                element = element.nextSibling;
            } while (element && element.className);

            if (element)
            {
                createAudio(element);
            }
            else
            {
                nixPlayer();
                location.hash = "album:" + release + "," + releaseName;
            }
        });

        element.className = "active";

        progress.value = 0;
        progress.innerHTML = "0 seconds";

        player.src = "/release/" + release + "/track/" + element.getAttribute("data-id");

        // For default selected tracks so hitting play doesn't revert to playing the first track.
        if (!preload)
        {
            setPlayButtonState(button, icons.play);
        }

        location.hash = "album:" + release + ",track:" + element.getAttribute("data-id") + "," + releaseName;
    };

    tracks.on("click", "> li", function (event)
    {
        if (event.currentTarget.className !== "disabled")
        {
            createAudio(event.currentTarget);
        }
    });

    function clickOnAlbumHandler (event)
    {

        // Disabled albums are unplayable.
        if (!event.currentTarget.className)
        {
            var r = event.currentTarget.getAttribute("data-id");

            // We're already listening to this album.
            if (release == r)
            {
                $("li[data-id='album']").click();
                return;
            }
            release = r;

            error.css("display", "");
            wait.hide();

            nixPlayer();
            tracks.empty();

            // Change pages.
            $("li[data-id='album']").removeClass("disabled").click();

            // They're all zeros until this page is shown so they can't be gotten ahead of time.
            pox = progress.getBoundingClientRect();
            mox = meter.getBoundingClientRect();

            // If after 1s were still waiting, show the throbber. Prevents a scrollbar from showing up
            // due to the throbber itself while the parent is still expanding.
            lastWaitTimeout = setTimeout(function ()
            {
                if (album.height() < 85)
                {
                    album.css("height", UC.fontSize * 2 + UC.inputHeight + 85 + "px");
                    album.data("height", album.height() + "px");
                }
                wait.show();
                wait.css("animationPlayState", "running");
            }, 1000);

            $.ajax("/release/" + release, {
                success: function (data)
                {
                    clearTimeout(lastWaitTimeout);
                    button.style.opacity = progress.style.opacity = meter.style.opacity = mute.style.opacity = "";

                    // Update page hash, so someone can use this URL to get back here.
                    releaseName = data.title.replace(/ /g, "_");

                    if (!event.track)
                    {
                        location.hash = "album:" + r + "," + releaseName;
                    }

                    info.css("display", "");
                    title.text(data.title);
                    artist.text(data.artist);
                    link.attr("href", "http://www.kahvi.org/releases.php?release_number=" +
                            new Array(4 - (data.id + "").length).join("0") + data.id);

                    tracks.empty();
                    tracks.append($.map(data.list, function (value, index)
                    {
                        var li = [ "<li data-id='", index + 1, "'" ];

                        li.push(value.valid ? "" : " title='This file cannot be played back as the server was unable to read it'");
                        li.push(value.valid ? "" : " class='disabled'");
                        li.push(">");

                        li.push("<span>", value.length == -1 ? "" : formatDuration(value.length), "</span>");
                        li.push("<span>", value.title, "</span></li>");

                        return li.join("");
                    }).join(""));
                    tracks.parent().scrollTop(0);

                    var height = Math.min(15.75, data.list.length) * UC.lineHeight + UC.inputHeight + !!data.list.length * UC.fontSize * 2.8 + "px";
                    album.data("height", height);
                    album.css("height", height);

                    wait.css("animationPlayState", "");
                    wait.hide();

                    // Select indicated track (for hash-given releases).
                    var track = album.find("[data-id='" + Number(event.track) + "']");

                    if (track.length)
                    {
                        track.addClass("active");
                        createAudio(track[0], true);
                    }
                },
                error: function (xhr, status)
                {
                    var height;
                    clearTimeout(lastWaitTimeout);

                    if (status !== "abort")
                    {
                        height = UC.fontSize * 2 + UC.inputHeight + 85 + "px";
                        album.css("height", height);
                        album.data("height", height);

                        wait.css("animationPlayState", "");
                        wait.hide();

                        info.css("display", "none");
                        tracks.empty();
                        error.css("display", "table");

                        button.style.opacity = progress.style.opacity = meter.style.opacity = mute.style.opacity = .2;
                        location.hash = "";
                    }
                }
            }); // ajax
        }
    }
    $("#search ul").on("click", "> li", clickOnAlbumHandler);

    // Look for special URL to auto-open to a release.
    releaseName = location.hash.match(/album:(\d+)|$/)[1];

    if (location.hash && releaseName)
    {
        // Just want to be in the queue after the interface figures out all the magic font/height values.
        setTimeout(function ()
        {
            clickOnAlbumHandler({
                currentTarget : {
                    getAttribute : function ()
                    {
                        return releaseName;
                    },
                    lastChild : {
                        innerHTML : location.hash.replace("#", "").replace(/^album:\d+(?:,track:\d+)?,?/, "")
                    }
                },
                track : location.hash.match(/,track:(\d+)|$/)[1]
            });
        }, 1050);
    }
});

function checkWaitStyles (element)
{
    // Display just the wait text if the browser doesn't support either animations or the gradients used.
    if (!element.css("backgroundImage") || !element.css("animationName"))
    {
        element.addClass("text");
    }
}

// IE
document.createElement("nav");
document.createElement("footer");
