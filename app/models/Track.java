package models;

import com.google.gson.annotations.Expose;

import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class Track extends Model implements Comparable<Track>
{
    public long release;

    public String name;

    @Expose
    public String title;

    @Expose
    public double length;

    public int index;

    @Expose
    public boolean valid;

    public Track (long release, String name, String title, int index)
    {
        this.release = release;
        this.name = name;
        this.title = title;
        length = -1;
        this.index = index;
        valid = true;
    }

    public Track (long release, int index)
    {
        this.release = release;
        title = "error reading file";
        length = -1;
        this.index = index;
        valid = false;
    }

    public int compareTo (Track track)
    {
        return index - track.index;
    }
}
