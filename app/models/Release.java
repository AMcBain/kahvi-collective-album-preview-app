package models;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import play.db.jpa.GenericModel;
import play.data.validation.MaxSize;

@Entity
public class Release extends GenericModel
{
    @Expose
    @Id
    @GeneratedValue
    public Long id;

    @Expose
    public String genre;

    @Expose
    public String artist;

    @Expose
    public String title;

    @Expose
    public int tracks;

    @Lob
    @Column(length=2000)
    public String search;

    @Expose
    public String format;

    public String url;

    public String type;

    @Expose
    @Transient
    public List<Track> list;
}
