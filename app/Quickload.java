import java.io.File;

import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import play.test.Fixtures;

import models.Release;

@Every("1d")
public class Quickload extends Job {

    public void doJob() {
        File file = new File(Play.applicationPath, "conf/quick-data.yml");

        if(file.exists()) {
            Fixtures.load("quick-data.yml");
            file.delete();
        }
    }

}
