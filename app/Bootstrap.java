import java.io.File;

import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

import models.Release;

@OnApplicationStart
public class Bootstrap extends Job {

    public void doJob() {

        if(Release.count() == 0 && new File(Play.applicationPath, "conf/initial-data.yml").exists()) {
            Fixtures.load("initial-data.yml");
        }
    }

}
