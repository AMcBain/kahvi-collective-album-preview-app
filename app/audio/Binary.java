package audio;

import java.io.InputStream;
import java.io.IOException;
import java.util.zip.ZipFile;

public class Binary extends InputStream
{
    private final InputStream stream;

    private final long length;

    private final ZipFile zip;

    private long position;
    private long mark;
    private long end = -1;

    protected Binary(InputStream stream, long length)
    {
        this.stream = stream;
        this.length = length;
        zip = null;
    }

    protected Binary(InputStream stream, long length, ZipFile zip)
    {
        this.stream = stream;
        this.length = length;
        this.zip = zip;
    }

    // Borrowed from Beaglebuddy MP3 library ( http://www.beaglebuddy.com/ ) to
    // work around http://bugs.sun.com/view_bug.do?bug_id=6204246 involving
    // InputStreams. [Lack of] license is compatible with this project. I have
    // to say, his solution was definitely better than my naive quick-fix. This
    // hasn't seemed to be a problem yet for this class, but that's no reason
    // not to be preventative.
    public Binary from(long start) throws IOException
    {
        while (start > 0)
        {
            long bytes = this.stream.skip(start);

            if (bytes == 0)
            {
                if (this.stream.read() == -1)
                {
                    break;
                }
                bytes = 1;
            }
            else if (bytes < 0)
            {
                // Looking at the InputStream and BufferedInputStream source, I
                // rather think the JVM would have to be in quite a wonky state
                // for skip to return a negative number. However, seeing as the
                // joneric (Beaglebuddy MP3) thinks it's exceptional, it stays.
                throw new IOException("The world has ended! stream.skip(" + start + ") returned a negative value: " + bytes);
            }
            start -= bytes;
        }
        return this;
    }

    /**
     * Inclusive
     */
    public Binary to(long end)
    {
        this.end = end;
        return this;
    }

    public int available () throws IOException
    {
        return stream.available();
    }

    public void close () throws IOException
    {
        try
        {
            stream.close();
        }
        finally
        {
            if (zip != null)
            {
                zip.close();
            }
        }
    }

    public void mark (int readlimit)
    {
        stream.mark(readlimit);
        mark = position;
    }

    public boolean markSupported ()
    {
        return stream.markSupported();
    }

    public int read () throws IOException
    {
        return stream.read();
    }

    public int read (byte[] b) throws IOException
    {
        return read(b, 0, b.length);
    }

    public int read (byte[] b, int off, int len) throws IOException
    {
        if (end == -1 || position < end)
        {
            int read;

            if (end == -1 || len < end + 1 - position)
            {
                read = stream.read(b, off, len);
            }
            else
            {
                byte[] c = new byte[(int)(end + 1 - position)];
                read = stream.read(c);
                System.arraycopy(c, 0, b, off, c.length);
            }

            position += read;
            return read;
        }
        return -1;
    }

    public void reset () throws IOException
    {
        stream.reset();
        position = mark;
    }

    public long skip (long n) throws IOException
    {
        long skipped;

        if (end == -1 || n < end + 1 - position)
        {
            skipped = stream.skip(n);
        }
        else if (position < end)
        {
            skipped = stream.skip(end + 1 - position);
        }
        else
        {
            skipped = 0;
        }

        position += skipped;
        return skipped;
    }

    public long length ()
    {
        return length;
    }
}
