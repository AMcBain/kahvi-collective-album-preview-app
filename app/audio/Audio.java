package audio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;

import audio.VorbisAudio;
import audio.MP3Audio;

import models.Release;
import models.Track;

public class Audio
{
    private static List<Track> getTrackFromFile (long id, File file, String format)
    {
        Track track = null;

        try
        {
            if ("mp3".equals(format))
            {
                track = MP3Audio.createTrack(id, file);
            }
            else
            {
                track = VorbisAudio.createTrack(id, file);
            }
            track.index = 1;
        }
        catch (Exception e)
        {
            track = new Track(id, 0);
        }

        List<Track> tracks = new ArrayList<Track>();
        tracks.add(track);
        return tracks;
    }

    private static List<Track> getTracksFromArchive (long id, File file, String format)
    {
        if (!file.exists())
        {
            return null;
        }

        List<Track> tracks =  new ArrayList<Track>();
        ZipFile zip = null;

        try
        {
            zip = new ZipFile(file);
            int index = 0;

            Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements())
            {
                try
                {
                    ZipEntry entry = entries.nextElement();

                    // Skip included images, text files, etc.
                    if (entry.getName().endsWith("." + format))
                    {
                        Track track;

                        if ("mp3".equals(format))
                        {
                            track = MP3Audio.createTrack(id, zip, entry);
                        }
                        else
                        {
                            track = VorbisAudio.createTrack(id, zip, entry);
                        }
                        index = track.index;

                        tracks.add(track);
                    }
                }
                catch(Exception e)
                {
                    // Sometimes reading certain elements fails. Empty track keeps proper track indexes when sorted.
                    index++;
                    tracks.add(new Track(id, index));
                }
            }
        }
        catch (IOException e)
        {
            // FIXME do something better
            e.printStackTrace();
        }
        finally
        {
            if (zip != null)
            {
                try
                {
                    zip.close();
                }
                catch (IOException e)
                {
                    // Uh, well ...
                }
            }
        }

        return tracks;
    }

    public static List<Track> getTracks (long id, File file, String format)
    {
        if (file.getName().endsWith(".zip"))
        {
            return getTracksFromArchive(id, file, format);
        }
        return getTrackFromFile(id, file, format);
    }

    public static Binary getTrack (File release, String track) throws IOException
    {
        if (!release.exists())
        {
            return null;
        }

        if (release.getName().endsWith(".zip"))
        {
            ZipFile file = new ZipFile(release);
            ZipEntry entry = file.getEntry(track);
            return new Binary(file.getInputStream(entry), entry.getSize(), file);
        }
        return new Binary(new FileInputStream(release), release.length());
    }
}
