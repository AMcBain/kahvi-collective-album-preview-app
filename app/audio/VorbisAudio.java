package audio;

import adamb.ogg.ErrorTolerantPageStream;
import adamb.ogg.LogicalPageStream;
import adamb.ogg.PacketSegmentStream;
import adamb.ogg.PacketStream;
import adamb.ogg.PhysicalPageStream;
import adamb.ogg.StreamSerialFilter;
import adamb.vorbis.CommentField;
import adamb.vorbis.VorbisCommentHeader;
import adamb.vorbis.VorbisIO;
import adamb.vorbis.VorbisPacketStream;

import java.io.BufferedInputStream;
import java.io.File;
import java.util.List;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;

import models.Track;

public class VorbisAudio
{
    // Were the data not captive, more could be done here such as handling fallbacks for track indexes. Kavhi does a very good job of providing
    // track indexes and titles, etc. So there's no need to care about unparseable track index, missing track indexes, missing titles ...
    private static Track createTrack (long id, String name, List<CommentField> fields)
    {
        String title = null;
        int track = -1;

        for (CommentField field : fields)
        {
            if (field.name != null)
            {
                if ("TITLE".equals(field.name.toUpperCase()))
                {
                    title = field.value;
                }
                else if ("TRACKNUMBER".equals(field.name.toUpperCase()))
                {
                    track = Integer.parseInt(field.value);
                }
            }
        }

        return new Track(id, name, title, track);
    }

    public static Track createTrack (long id, File file) throws Exception
    {
        return createTrack(id, file.getName(), VorbisIO.readComments(file).fields);
    }

    public static Track createTrack (long id, ZipFile zip, ZipEntry entry) throws Exception
    {
        PhysicalPageStream pps = new PhysicalPageStream(new BufferedInputStream(zip.getInputStream(entry), 256 * 1024));
        LogicalPageStream lps = new StreamSerialFilter(new ErrorTolerantPageStream(pps), false);
        VorbisPacketStream vps = new VorbisPacketStream(new PacketStream(new PacketSegmentStream(lps)));
        vps.next();
        vps.next();

        return createTrack(id, entry.getName(), vps.getCommentHeader().fields);
    }
}
