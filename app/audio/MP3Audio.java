package audio;

import com.beaglebuddy.mp3.MP3;

import java.io.File;
import java.net.URL;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;

import protocol.zip.Handler;

import models.Track;

public class MP3Audio
{
    public static Track createTrack (long id, File file) throws Exception
    {
        MP3 mp3 = new MP3(file);
        return new Track(id, file.getName(), mp3.getTitle(), mp3.getTrack());
    }

    public static Track createTrack (long id, ZipFile zip, ZipEntry entry) throws Exception
    {
        String name = entry.getName();
        MP3 mp3 = new MP3(new URL(null, "zip:" + name, new Handler(zip)));
        return new Track(id, name, mp3.getTitle(), mp3.getTrack());
    }
}
