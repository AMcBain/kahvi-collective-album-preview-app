package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import play.mvc.Controller;
import play.mvc.Http.Header;
import play.Play;

import audio.Audio;
import audio.Binary;

import models.Release;
import models.Track;

import protocol.zip.Handler;
import protocol.Unsatisfiable;

public class Application extends Controller
{
    private static Object RELEASE_LOCK = new Object();

    public static void index ()
    {
        render();
    }

    public static void search (String value)
    {
        if (value == null || value.isEmpty())
        {
            renderJSON(Collections.emptyList());
        }

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        List<Release> releases = Release.find("bySearchIlike", "%" + value + "%").fetch();
        renderJSON(gson.toJson(releases));
    }

    private static File getReleasePath (String url)
    {
        return new File(new File(Play.applicationPath, "releases"), url.substring(url.lastIndexOf("/") + 1));
    }

    public static void release (long id)
    {
        Release release = null;
        List<Track> tracks = Collections.emptyList();

        synchronized (RELEASE_LOCK)
        {
            release = Release.find("byId", id).first();
            notFoundIfNull(release, id <= 0 ? "No release found with that ID." : "Release not pulled and cached yet.");

            tracks = Track.find("release = ? order by index", id).fetch();
            if (tracks.isEmpty())
            {
                tracks = Audio.getTracks(release.id, getReleasePath(release.url), "mpeg".equals(release.format) ? "mp3" : release.format);
                notFoundIfNull(tracks, "Release not pulled and cached yet.");

                for (int i = 0; i < tracks.size(); i++)
                {
                    tracks.get(i).save();
                }
            }
        }

        Collections.sort(tracks);
        release.list = tracks;

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        renderJSON(gson.toJson(release));
    }

    public static void track (long id, int index) throws IOException
    {
        Release release = Release.find("byId", id).first();
        notFoundIfNull(release, id <= 0 ? "No release found with that ID." : "Release metadata not stored yet.");

        Track track = Track.find("byReleaseAndIndex", id, index).first();
        notFoundIfNull(track, index <= 0 ? "Track numbers start with 1, dummy." : "Release metadata not stored yet.");

        if (track.name == null)
        {
            error("Unable to read file.");
        }

        response.contentType = "audio/" + release.format;
        response.setHeader("Accept-Ranges", "bytes");

        // The good stuff? Depends on your tastes.
        Binary binary = Audio.getTrack(getReleasePath(release.url), track.name);

        // Yes, we support range requests! Forgive me if the following is naive and incompetent.
        // Why am I writing my own range support? Play 1.2.5 doesn't support range requests. If
        // it did, I would probably be using that. However, I wouldn't expect it to be any more
        // performant than the following. This implementation also does not support requests for
        // multiple data ranges. It just wasn't needed to handle requests made for audio tags.
        if (request.headers.containsKey("range"))
        {
            // Check for a value.
            String value = request.headers.get("range").value();

            if (value != null && !value.isEmpty())
            {
                int eq = value.indexOf("=");

                // Valid range?
                if (eq == 5)
                {
                    String[] parts = value.substring(eq + 1).split("-");
                    long start = -1;
                    long end = -1;

                    if (parts.length > 0)
                    {
                        start = Long.parseLong(parts[0]);
                    }
                    if (parts.length > 1)
                    {
                        end = Long.parseLong(parts[0]);
                    }

                    // Open-ended request.
                    if (start != -1 && end == -1)
                    {
                        end = binary.length() - 1;
                    }
                    end = Math.min(end, binary.length() - 1);

                    if (end >= start)
                    {
                        response.status = 206;
                        response.setHeader("Content-Length", end + 1 - start + "");
                        response.setHeader("Content-Range", start + "-" + end + "/" + binary.length() + "");

                        renderBinary(binary.from(start).to(end));
                    }
                    else
                    {
                        throw new Unsatisfiable();
                    }
                }
            }
        }

        renderBinary(binary);
    }

    public static void length (long id, int index, double length)
    {
        if (length > 0)
        {
            Track track = Track.find("byReleaseAndIndex", id, index).first();
            notFoundIfNull(track, "Release metadata not stored yet.");

            track.length = length;
            track.save();
        }
        else
        {
            response.status = 400;
            renderText("Track length must be a number greater than zero.");
        }
    }
}
