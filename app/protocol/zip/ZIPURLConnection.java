package protocol.zip;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZIPURLConnection extends HttpURLConnection
{
    private ZipFile zip;
    private ZipEntry entry;

    protected ZIPURLConnection (ZipFile zip, URL url) throws MalformedURLException, IOException
    {
        super(url);

        this.zip = zip;
        entry = zip.getEntry(this.url.getFile());

        if (entry == null)
        {
            throw new IOException("Not a valid file in ZIP: " + url);
        }
    }

    @Override
    public boolean usingProxy ()
    {
        return false;
    }

    @Override
    public void connect () throws IOException
    {
    }

    @Override
    public void disconnect ()
    {
    }

    @Override
    public InputStream getInputStream () throws IOException
    {
        return zip.getInputStream(entry);
    }

    public int getContentLength ()
    {
        return (int)entry.getSize();
    }
}
