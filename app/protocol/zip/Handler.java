package protocol.zip;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.zip.ZipFile;

public class Handler extends URLStreamHandler
{
    private ZipFile zip;

    public Handler (ZipFile zip)
    {
        this.zip = zip;
    }

    @Override
    protected URLConnection openConnection(URL url) throws IOException
    {
        return new ZIPURLConnection(zip, url);
    }
}
