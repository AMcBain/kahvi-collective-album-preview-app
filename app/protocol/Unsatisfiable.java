package protocol;

import play.exceptions.UnexpectedException;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.Result;

public class Unsatisfiable extends Result
{
    public Unsatisfiable()
    {
        super("Requested Range Not Satisfiable");
    }

    public void apply(Request request, Response response)
    {
        response.status = 416;
        try
        {
            response.out.write(getMessage().getBytes(getEncoding()));
        }
        catch (Exception e) {
            throw new UnexpectedException(e);
        }
    }
}
